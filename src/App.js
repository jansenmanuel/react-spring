import React from 'react';
import './App.css';
import { Switch, Route } from "react-router-dom";
import Home from './Components/Home';
import Login from './Components/Login';
import Register from './Components/Register';
import AddData from './Components/AddData';
import EditData from './Components/EditData';
import Karyawan from './Components/Karyawan';
import Search from './Components/Search';

function App() {
  return (
    <div>
      <Switch>
        <Route path="/add" component={AddData} />
        <Route path="/edit" component={EditData} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/karyawan" component={Karyawan} />
        <Route path="/search" component={Search} />
        <Route path="/" component={Home} />
      </Switch>
    </div>
  );
}

export default App;