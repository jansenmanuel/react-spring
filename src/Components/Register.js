import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class Register extends Component {
    render() {
        return (
            <>
                <div className="container">
                    <div className="jumbotron col-4 my-5 mx-auto shadow p-3 rounded">
                        <form className="form" action="#" method="post">
                            <h4 className="text-center">Register</h4>
                            <hr />
                            <label>Nama</label>
                            <input type="nama" className="form-control" name="nama" id="nama" placeholder="Masukan nama..." autoFocus />
                            <label>Username</label>
                            <input type="username" className="form-control" name="username" id="username" placeholder="Masukan username..." />
                            <label>Password</label>
                            <input type="password" className="form-control" name="password" id="password" placeholder="Masukan password..." />
                            <br />
                            <Link to='/login'>
                                <button className="btn btn-success btn-sm" type="submit">Register</button>
                            </Link>
                        </form>
                    </div>
                </div>
            </>
        )
    }
}
