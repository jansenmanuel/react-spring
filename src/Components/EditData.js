import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class employees extends React.Component {
    state = {
        nik: '',
        nama: '',
        ttl: '',
        jk: '',
        gd: '',
        alamat: '',
        rt: '',
        rw: '',
        kel: '',
        kec: '',
        agama: '',
        pkr: '',
        sts: '',
        kwn: '',
    }
    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit = event => {
        event.preventDefault();

        console.log(this.state)
        axios.post(`http://localhost:8181/update`, this.state)
            .then(res => {
                console.log(res)
                console.log(res.data);
                alert('Berhasil mengedit data!')
            })
    }

    render() {
        return (
            <div className="addData">
                <div className="container">
                    <h3 className="judul mt-3">Edit Data Karyawan</h3>
                    <form onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="col">
                                <label>NIK</label><br></br>
                                <input type="number" className="form-control" id="nik" name="nik" onChange={this.handleChange} /><br></br>
                            </div>
                            <div className="col">
                                <label>Nama</label><br></br>
                                <input type="text" className="form-control" id="nama" nama="nama" onChange={this.handleChange} /><br></br>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <label>Tempat / Tgl Lahir</label><br></br>
                                <input type="text" className="form-control" id="ttl" name="ttl" onChange={this.handleChange} /><br></br>
                            </div>

                            <div className="col">
                                <label>Jenis Kelamin</label><br></br>
                                <input type="text" className="form-control" id="jk" name="jk" onChange={this.handleChange} /><br></br>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <label>Golongan Darah</label><br></br>
                                <input type="text" className="form-control" id="gd" name="gd" onChange={this.handleChange} /><br></br>
                            </div>

                            <div className="col">
                                <label>Alamat</label><br></br>
                                <input type="text" className="form-control" id="alamat" name="alamat" onChange={this.handleChange} /><br></br>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <label>RT</label><br></br>
                                <input type="number" className="form-control" id="rt" name="rt" onChange={this.handleChange} /><br></br>
                            </div>

                            <div className="col">
                                <label>RW</label><br></br>
                                <input type="number" className="form-control" id="rw" name="rw" onChange={this.handleChange} /><br></br>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <label>Kelurahan</label><br></br>
                                <input type="text" className="form-control" id="kel" name="kel" onChange={this.handleChange} /><br></br>
                            </div>

                            <div className="col">
                                <label>Kecamatan</label><br></br>
                                <input type="text" className="form-control" id="kec" name="kec" onChange={this.handleChange} /><br></br>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <label>Agama</label><br></br>
                                <input type="text" className="form-control" id="agama" name="agama" onChange={this.handleChange} /><br></br>
                            </div>

                            <div className="col">
                                <label>Status Perkawinan</label><br></br>
                                <input type="text" className="form-control" id="sts" name="sts" onChange={this.handleChange} /><br></br>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <label>Pekerjaan</label><br></br>
                                <input type="text" className="form-control" id="pkr" name="pkr" onChange={this.handleChange} /><br></br>
                            </div>

                            <div className="col">
                                <label>Kewarganegaraan</label><br></br>
                                <input type="text" className="form-control" id="kwn" name="kwn" onChange={this.handleChange} /><br></br>
                            </div>
                        </div>

                        <button className="btn btn-success mx-1 mb-2 float-right">Edit</button>
                        <Link to='/karyawan'>
                            <button className="btn btn-secondary mx-1 mb-2 float-right">Back</button>
                        </Link>
                    </form>
                </div>
            </div >
        )
    }

}