import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class Login extends Component {
    render() {
        return (
            <>
                <div className="container">
                    <div className="jumbotron col-4 my-5 mx-auto shadow p-3 rounded">
                        <form className="form" action="#" method="post">
                            <h4 className="text-center">Login</h4>
                            <hr /><br />
                            <label>Username</label><br />
                            <input type="username" className="form-control" name="username" id="username" placeholder="Masukan username..." autoFocus /><br />
                            <label>Password</label><br />
                            <input type="password" className="form-control" name="password" id="password" placeholder="Masukan password..." /><br />
                            <Link to='/karyawan'>
                                <button className="btn btn-primary btn-sm mx-1" type="submit">Login</button>
                            </Link>
                            <Link to='/register'>
                                <button className="btn btn-success btn-sm mx-1" type="submit">Register</button>
                            </Link>
                            <Link to='/'>
                                <button className="btn btn-info btn-sm mx-1 float-right" type="submit">Home</button>
                            </Link>
                        </form>
                    </div>
                </div>
            </>
        )
    }
}
