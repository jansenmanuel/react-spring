import React, { Component } from 'react';
import Axios from 'axios';
import Navbar from './Navbar';
export default class Home extends Component {
    state = {
        user: [],
    }

    componentDidMount() {
        Axios.get('http://localhost:8181/employees')
            .then(res => {
                const user = res.data;
                this.setState({ user });
            })
    }

    render() {
        return (
            <>
                <Navbar />
                <div className="home">
                    <div className="container mt-3">
                        <h3 className="text-center">Data Karyawan</h3>
                        <table className="table table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>Tempat / Tgl Lahir</th>
                                    <th>Jenis-kelamin</th>
                                    <th>Golongan Darah</th>
                                    <th>Alamat</th>
                                    <th>RT</th>
                                    <th>RW</th>
                                    <th>Kelurahan</th>
                                    <th>Kecamatan</th>
                                    <th>Status Perkawinan</th>
                                    <th>Pekerjaan</th>
                                    <th>Kewarganeragaan</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.user.map(user =>
                                        <tr key={user.id}>
                                            <td>{user.nik}</td>
                                            <td>{user.nama}</td>
                                            <td>{user.ttl}</td>
                                            <td>{user.jk}</td>
                                            <td>{user.gd}</td>
                                            <td>{user.alamat}</td>
                                            <td>{user.rt}</td>
                                            <td>{user.rw}</td>
                                            <td>{user.kel}</td>
                                            <td>{user.kec}</td>
                                            <td>{user.sts}</td>
                                            <td>{user.pkr}</td>
                                            <td>{user.kwn}</td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </>
        );
    }
}
