import React, { Component } from 'react'

export default class Search extends Component {
    onChangeSearchData(e) {
        const searchData = e.target.value.toLowerCase();

        this.setState({
            searchData: searchData
        });
    }

    searchData() {
        const searchByName = this.state.persons.filter(searchData => searchData.name.toLowerCase().includes(this.state.searchData))
        console.log(searchByName)
        this.setState({
            persons: searchByName
        });
    }

    render() {

        const { searchData, error, user } = this.state;
        if (error) {
            return (
                <>Error:{error.message}</>
            )
        }
        else {
            return (
                <>
                    <div className="row">
                        <input className="form-control mr-sm-2 col-7 ml-4" type="search" value={searchData} placeholder="Search By Name" aria-label="Search" onChange={this.onChangeSearchData} />
                        <button className="btn btn-success my-2 my-sm-0 " type="submit" onClick={this.searchData}>Search</button>
                    </div>
                </>
            )
        };
    }
}