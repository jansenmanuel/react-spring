import React, { Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import Navbar2 from './Navbar2';
// import Search from './Search';
export default class Karyawan extends Component {
    state = {
        user: [],
    }

    componentDidMount() {
        Axios.get('http://localhost:8181/employees')
            .then(res => {
                const user = res.data;
                this.setState({ user });
            })
    }

    handleDelete = id => {
        Axios.delete(`http://localhost:8181/delete/` + id)
            .then(res => {
                console.log(res);
                console.log(res.data);
                window.location.reload();
                alert('Berhasil menghapus data!')
            })
    }

    render() {
        return (
            <>
                <Navbar2 />
                {/* <Search /> */}
                <div className="karyawan">
                    <div className="container mt-3">
                        <h3 className="text-center">Data Karyawan</h3>
                        {/* <Seach /> */}
                        <Link to='/add'>
                            <button className="addBtn float-right mb-2 btn btn-success" variant="contained">Add Data</button>
                        </Link>
                        <table className="table table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>Tempat / Tgl Lahir</th>
                                    <th>Jenis-kelamin</th>
                                    <th>Golongan Darah</th>
                                    <th>Alamat</th>
                                    <th>RT</th>
                                    <th>RW</th>
                                    <th>Kelurahan</th>
                                    <th>Kecamatan</th>
                                    <th>Status Perkawinan</th>
                                    <th>Pekerjaan</th>
                                    <th>Kewarganeragaan</th>
                                    <th>Setting</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.user.map(user =>
                                        <tr key={user.id}>
                                            <td>{user.nik}</td>
                                            <td>{user.nama}</td>
                                            <td>{user.ttl}</td>
                                            <td>{user.jk}</td>
                                            <td>{user.gd}</td>
                                            <td>{user.alamat}</td>
                                            <td>{user.rt}</td>
                                            <td>{user.rw}</td>
                                            <td>{user.kel}</td>
                                            <td>{user.kec}</td>
                                            <td>{user.sts}</td>
                                            <td>{user.pkr}</td>
                                            <td>{user.kwn}</td>
                                            <td>
                                                <Link to='/edit'>
                                                    <button className="btn btn-warning btn-sm mx-1 my-1">Edit </button>
                                                </Link>
                                                <button className="btn btn-danger btn-sm mx-1 my-1" onClick={() => this.handleDelete(user.id)}>Hapus</button>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </>
        );
    }
}
