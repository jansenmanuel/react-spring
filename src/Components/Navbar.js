import React from 'react';
import { Link } from 'react-router-dom';

export default function Navbar() {

    return (
        <>
            <div>
                <nav className="navbar navbar-light bg-dark">
                    <Link to="/">
                        <p className="navbar-brand mx-1 text-light my-auto">Karyawan</p>
                    </Link>
                    <Link to="/login">
                        <button className="btn btn-primary btn-sm mx-1 float-right">Login</button>
                    </Link>
                </nav>
            </div>
        </>
    );
}
