import React from 'react';
import { Link } from 'react-router-dom';

export default function Navbar2() {

    return (
        <>
            <div>
                <nav className="navbar navbar-light bg-dark">
                    <Link to="/karyawan">
                        <p className="navbar-brand mx-1 text-light my-auto">Karyawan</p>
                    </Link>
                    <Link to="/login">
                        <button className="btn btn-danger btn-sm mx-1 float-right">Logout</button>
                    </Link>
                </nav>
            </div>
        </>
    );
}
