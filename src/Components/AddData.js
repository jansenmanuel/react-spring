import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class employees extends React.Component {
    state = {
        nik: '',
        nama: '',
        ttl: '',
        jk: '',
        gd: '',
        alamat: '',
        rt: '',
        rw: '',
        kel: '',
        kec: '',
        agama: '',
        pkr: '',
        sts: '',
        kwn: '',
    }
    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit = event => {
        event.preventDefault();

        console.log(this.state)
        axios.post(`http://localhost:8181/addEmployee`, this.state)
            .then(res => {
                console.log(res)
                console.log(res.data)
                alert('Berhasil menambah data!')
            })
    }

    render() {
        return (
            <div className="addData">
                <div className="container">
                    <div className="card col-md-8 my-3 mx-auto">
                        <div className="card-header">
                            <h3 className="judul text-center">Tambah Data Karyawan</h3>
                        </div>
                        <div className="card-body">
                            <form onSubmit={this.handleSubmit}>
                                <div className="row">
                                    <div className="col">
                                        <label>NIK</label>
                                        <input type="number" className="form-control" id="nik" name="nik" placeholder="Input NIK..." onChange={this.handleChange} />
                                    </div>
                                    <div className="col">
                                        <label>Nama</label>
                                        <input type="text" className="form-control" id="nama" nama="nama" placeholder="Input Nama..." onChange={this.handleChange} />
                                    </div>
                                </div>
                                <br />

                                <div className="row">
                                    <div className="col">
                                        <label>Tempat / Tgl Lahir</label>
                                        <input type="text" className="form-control" id="ttl" name="ttl" placeholder="Input Tempat/Tgl Lahir..." onChange={this.handleChange} />
                                    </div>

                                    <div className="col">
                                        <label>Jenis Kelamin</label>
                                        <input type="text" className="form-control" id="jk" name="jk" placeholder="Input Jenis Kelamin..." onChange={this.handleChange} />
                                    </div>
                                </div>
                                <br />

                                <div className="row">
                                    <div className="col">
                                        <label>Golongan Darah</label>
                                        <input type="text" className="form-control" id="gd" name="gd" placeholder="Input Golongan Darah..." onChange={this.handleChange} />
                                    </div>

                                    <div className="col">
                                        <label>Alamat</label>
                                        <input type="text" className="form-control" id="alamat" name="alamat" placeholder="Input Alamat..." onChange={this.handleChange} />
                                    </div>
                                </div>
                                <br />

                                <div className="row">
                                    <div className="col">
                                        <label>RT</label>
                                        <input type="number" className="form-control" id="rt" name="rt" placeholder="Input Nomor RT..." onChange={this.handleChange} />
                                    </div>

                                    <div className="col">
                                        <label>RW</label>
                                        <input type="number" className="form-control" id="rw" name="rw" placeholder="Input Nomor RT..." onChange={this.handleChange} />
                                    </div>
                                </div>
                                <br />

                                <div className="row">
                                    <div className="col">
                                        <label>Kelurahan</label>
                                        <input type="text" className="form-control" id="kel" name="kel" placeholder="Input Kelurahan..." onChange={this.handleChange} />
                                    </div>

                                    <div className="col">
                                        <label>Kecamatan</label>
                                        <input type="text" className="form-control" id="kec" name="kec" placeholder="Input Kecamatan..." onChange={this.handleChange} />
                                    </div>
                                </div>
                                <br />

                                <div className="row">
                                    <div className="col">
                                        <label>Agama</label>
                                        <input type="text" className="form-control" id="agama" name="agama" placeholder="Input Agama..." onChange={this.handleChange} />
                                    </div>

                                    <div className="col">
                                        <label>Status Perkawinan</label>
                                        <input type="text" className="form-control" id="sts" name="sts" placeholder="Input Status Perkawinan..." onChange={this.handleChange} />
                                    </div>
                                </div>
                                <br />

                                <div className="row">
                                    <div className="col">
                                        <label>Pekerjaan</label>
                                        <input type="text" className="form-control" id="pkr" name="pkr" placeholder="Input Pekerjaan..." onChange={this.handleChange} />
                                    </div>
                                    <br />

                                    <div className="col">
                                        <label>Kewarganegaraan</label>
                                        <input type="text" className="form-control" id="kwn" name="kwn" placeholder="Input Kewarganegaraan..." onChange={this.handleChange} />
                                    </div>
                                </div>
                                <br />
                                <button className="btn btn-success mx-1 mb-2 float-right">Tambah</button>
                                <Link to='/karyawan'>
                                    <button className="btn btn-secondary mx-1 mb-2 float-right">Back</button>
                                </Link>
                            </form>
                        </div>
                    </div>
                </div>
            </div >
        )
    }

}